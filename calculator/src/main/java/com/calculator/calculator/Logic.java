package com.calculator.calculator;

import java.util.Scanner;

public class Logic {
	
	Scanner scan = new Scanner(System.in);
	
	private int zahl1;
	private int zahl2;
	private boolean stop = false;
	
	public void startCalculator () {
		while (stop == false) {
		System.out.println("Welcome to a very useless calculator");
		System.out.println("Please enter the first number:");
		zahl1 = scan.nextInt();
		scan.nextLine();
		System.out.println("Please enter the second number:");
		zahl2 = scan.nextInt();
		scan.nextLine();
		int result = calculate(zahl1, zahl2);
		System.out.println("The sum of both number is "+ result);
		System.out.println("Do you wanna start again? Y/N");
		String again = scan.nextLine().toLowerCase();
		if (again.equals("n")) {
			stop = true;
			System.out.println("Goodbye!");
		}
		
		}
		
	}
	
	public int calculate (int first, int second) {
		int result = first +second;
		
		return result;
		
	}

}
